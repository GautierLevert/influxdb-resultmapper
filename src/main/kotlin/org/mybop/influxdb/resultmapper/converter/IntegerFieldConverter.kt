package org.mybop.influxdb.resultmapper.converter

interface IntegerFieldConverter<T : Any?> : FieldConverter<T, Long?, Double?>
