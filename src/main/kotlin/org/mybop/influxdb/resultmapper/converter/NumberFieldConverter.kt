package org.mybop.influxdb.resultmapper.converter

interface NumberFieldConverter<T> : FieldConverter<T, Double?, Double?>
